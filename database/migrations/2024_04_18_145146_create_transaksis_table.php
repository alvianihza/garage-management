<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->id();
            $table->string("name_customer");
            $table->string("brand_kendaraan");
            $table->string("tipe");
            $table->timestamp("tanggal_pengerjaan");
            $table->string("jenis_pembayaran");
            $table->integer("value_dp");
            $table->integer("diskon");
            $table->integer("grand_total");
            $table->integer("kekurangan");
            $table->text("pengerjaan");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaksis');
    }
};
