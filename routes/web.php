<?php

use App\Http\Controllers\CustomersController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\KendaraansController;
use App\Http\Controllers\SparepartController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PencatatansController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'middleware' => ['auth'], 'as' => 'admin.'], function () {
});
Route::get('login', function () {
    return view('login');
})->name('login');

Route::get('login', function () {
    return view('login');
})->name('login');

Route::post('/login-proses', [LoginController::class, 'login_proses'])->name('login-proses');

Route::get('forgetpassword', function () {
    return view('forgetpassword');
})->name('forgetpassword');

Route::get('register', function () {
    return view('register');
})->name('register');

Route::get('dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::controller(TransaksiController::class)->prefix('transaksi')->group(function() {
    Route::get('', 'index')->name('transaksis');
    Route::get('insert', 'add')->name('transaksis.insert');
    Route::post('store', 'store')->name('transaksis.store');
    Route::put('update/{id}', 'update')->name('transaksis.update');
    Route::get('/edit/{id}', 'edit')->name('transaksis.edit');
    Route::get('/view/{id}', 'view')->name('transaksis.view');
});

Route::controller(PencatatansController::class)->prefix('pencatatans')->group(function() {
    Route::get('', 'index')->name('pencatatans');
    Route::get('insert', 'add')->name('pencatatans.insert');
    Route::post('store', 'store')->name('pencatatans.store');
    Route::get('/edit/{id}', 'edit')->name('pencatatans.edit');
    Route::delete('/pencatatans/{id}', 'delete')->name('pencatatans.delete');
});

Route::controller(CustomersController::class)->prefix('customers')->group(function() {
    Route::get('', 'index')->name('customers');
    Route::get('insert', 'add')->name('customers.insert');
    Route::post('store', 'store')->name('customers.store');
    Route::get('/edit/{id}', 'edit')->name('customers.edit');
    Route::put('/update/{id}', 'update')->name('customers.update');
    Route::delete('/delete/{id}', 'delete')->name('customers.delete');
});

Route::controller(SparepartController::class)->prefix('sparepart')->group(function() {
    Route::get('', 'index')->name('spareparts');
    Route::get('insert', 'add')->name('spareparts.insert');
    Route::post('store', 'store')->name('spareparts.store');
    Route::get('/edit/{id}', 'edit')->name('spareparts.edit');
    Route::put('/update/{id}', 'update')->name('spareparts.update');
    Route::delete('/spareparts/{id}', 'delete')->name('spareparts.delete');
});

Route::controller(KendaraansController::class)->prefix('kendaraans')->group(function() {
    Route::get('', 'index')->name('kendaraans');
    Route::get('insert', 'add')->name('kendaraans.insert');
    Route::post('store', 'store')->name('kendaraans.store');
    Route::get('/edit/{id}', 'edit')->name('kendaraans.edit');
    Route::put('/update/{id}', 'update')->name('kendaraans.update');
    Route::delete('/kendaraans/{id}', 'delete')->name('kendaraans.delete');
});

Route::controller(UserController::class)->prefix('users')->group(function() {
    Route::get('', 'index')->name('users');
    Route::get('insert', 'add')->name('users.insert');
    Route::post('store', 'store')->name('users.store');
    Route::get('/edit/{id}', 'edit')->name('users.edit');
    Route::put('/update/{id}', 'update')->name('users.update');
    Route::delete('/users/{id}', 'delete')->name('users.delete');
});