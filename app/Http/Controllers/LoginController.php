<?php

namespace App\Http\Controllers;

use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    //

    // Menampilkan formulir login
    public function showLoginForm()
    {
        return view('auth.login');
    }

    // Memproses login
    public function login(Request $request)
    {
        // Validasi input
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Coba untuk melakukan login
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {


            // Login berhasil
            return redirect()->intended('/dashboard'); // Redirect ke halaman setelah login
        } else {
            // Login gagal
            return back()->withErrors(['email' => 'Email atau password salah']); // Kembali ke halaman login dengan pesan kesalahan
        }
    }

    public function login_proses(Request $request)
    {
        $request->validate([
            'email'     => 'required',
            'password'  => 'required',
        ]);

        $data = [
            'email'     => $request->email,
            'password'  => $request->password
        ];

        if (Auth::attempt($data)) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('login')->with('failed', 'Email atau Password Salah');
        }
    }
}
