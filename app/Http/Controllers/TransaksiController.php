<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use mysqli;

class TransaksiController extends Controller
{
    public function index()
    {
        return view('transaksis.index', [
            'transaksis' => Transaksi::latest()->get()
        ]);
    }

    public function add()
    {
        return view('transaksis.insert', [
            'transaksis' => Transaksi::latest()->get()
        ]);
    }

    public function store()
    {
        return view('transaksis.store', [
            'transaksis' => Transaksi::latest()->get()
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = Transaksi::find($id);

        $created_at = date("Y-m-d H:i:s", strtotime($data->created_at));

        $data['jenis_pembayaran'] = 'Lunas';
        $data['value_dp'] = 0;
        $data['grand_total'] = $data['kekurangan'];
        $data['kekurangan'] = 0;

        $data->timestamps = false;
        $data['created_at'] = $created_at;
        $data['updated_at'] = now();

        $dataArray = $data->toArray();

        Transaksi::whereId($id)->update($dataArray);

        $keterangan = 'Pelunasan Pengerjaan Mobil ' . $data['name_customer'] . ' ( ' . $data['brand_kendaraan'] . $data['tipe'] . ' ) ';
        $income =  $data['grand_total'];

        $conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
        $second = $conn->prepare("INSERT into pencatatans( keterangan, income, created_at) 
        VALUES(?, ?, NOW())");
        $second->bind_param("si", $keterangan, $income);
        $second->execute();
        $second->close();

        return redirect()->route('transaksis');
    }

    public function view(Request $request, $id)
    {
        $data = Transaksi::find($id);

        return view('transaksis.view', compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data = Transaksi::find($id);

        return view('transaksis.edit', compact('data'));
    }
}
