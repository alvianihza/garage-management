<?php

namespace App\Http\Controllers;

use App\Models\Sparepart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SparepartController extends Controller
{
    public function index()
    {
        return view('spareparts.index', [
            'spareparts' => Sparepart::latest()->get()
        ]);
    }

    public function add()
    {
        return view('spareparts.insert', [
            'spareparts' => Sparepart::latest()->get()
        ]);
    }

    public function store()
    {
        return view('spareparts.store', [
            'spareparts' => Sparepart::latest()->get()
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = Sparepart::find($id);

        return view('spareparts.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Sparepart::find($id);
        $validator = Validator::make($request->all(), [
            'nama_sparepart'    => 'required',
            'brand_kendaraan'   => 'required',
            'tipe'              => 'required',
            'stok'              => 'required',
            'modal'             => 'required',
            'jual'              => 'required',
            'untung'            => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $created_at = date("Y-m-d H:i:s", strtotime($data->created_at));

        $data['nama_sparepart'] = $request->nama_sparepart;
        $data['brand_kendaraan'] = $request->brand_kendaraan;
        $data['tipe'] = $request->tipe;
        $data['stok'] = $request->stok;
        $data['modal'] = $request->modal;
        $data['jual'] = $request->jual;
        $data['untung'] = $request->untung;
        
        $data->timestamps = false;
        $data['created_at'] = $created_at;
        $data['updated_at'] = now();

        $dataArray = $data->toArray();

        Sparepart::whereId($id)->update($dataArray);

        return redirect()->route('spareparts');
    }

    public function delete(Request $request, $id)
    {
        $data = Sparepart::find($id);

        if ($data) {
            $data->forceDelete();
        }

        return redirect()->route('spareparts');
    }
}
