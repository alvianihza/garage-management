<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index', [
            'users' => User::latest()->get()
        ]);
    }

    public function add()
    {
        return view('users.insert', [
            'users' => User::latest()->get()
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'email'     => 'required',
        ]);
        
        return view('users.store', [
            'users' => User::latest()->get()
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = User::find($id);

        return view('kendaraans.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = User::find($id);
        $validator = Validator::make($request->all(), [
            'brand_kendaraan' => 'required',
            'tipe'            => 'required',
            'name_customer'   => 'required',
            'customer_name'   => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $created_at = date("Y-m-d H:i:s", strtotime($data->created_at));

        $data['brand_kendaraan'] = $request->brand_kendaraan;
        $data['tipe'] = $request->tipe;
        $data['name_customer'] = $request->name_customer;
        $data['customer_name'] = $request->customer_name;
        
        $data->timestamps = false;
        $data['created_at'] = $created_at;
        $data['updated_at'] = now();

        $dataArray = $data->toArray();

        User::whereId($id)->update($dataArray);

        return redirect()->route('kendaraans');
    }

    public function delete(Request $request, $id)
    {
        $data = User::find($id);

        if ($data) {
            $data->forceDelete();
        }

        return redirect()->route('kendaraans');
    }
}
