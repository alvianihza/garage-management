<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomersController extends Controller
{
    public function index()
    {
        return view('customers.index', [
            'customers' => Customer::latest()->get()
        ]);
    }

    public function add()
    {
        return view('customers.insert', [
            'customers' => Customer::latest()->get()
        ]);
    }

    public function store()
    {
        return view('customers.store', [
            'customers' => Customer::latest()->get()
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = Customer::find($id);

        return view('customers.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Customer::find($id);
        $validator = Validator::make($request->all(), [
            'nama'          => 'required',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required',
            'email'         => 'required|email',
            'alamat'        => 'required',
            'no_telp'       => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $created_at = date("Y-m-d H:i:s", strtotime($data->created_at));

        $data['nama'] = $request->nama;
        $data['jenis_kelamin'] = $request->jenis_kelamin;
        $data['tanggal_lahir'] = $request->tanggal_lahir;
        $data['email'] = $request->email;
        $data['alamat'] = $request->alamat;
        $data['no_telp'] = $request->no_telp;
        
        $data->timestamps = false;
        $data['created_at'] = $created_at;
        $data['updated_at'] = now();

        $dataArray = $data->toArray();

        Customer::whereId($id)->update($dataArray);

        return redirect()->route('customers');
    }

    public function delete(Request $request, $id)
    {
        $data = Customer::find($id);

        if ($data) {
            $data->forceDelete();
        }

        return redirect()->route('customers');
    }
}
