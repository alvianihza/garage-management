<?php

namespace App\Http\Controllers;

use App\Models\Kendaraans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KendaraansController extends Controller
{
    public function index()
    {
        return view('kendaraans.index', [
            'kendaraans' => Kendaraans::latest()->get()
        ]);
    }

    public function add()
    {
        return view('kendaraans.insert', [
            'kendaraans' => Kendaraans::latest()->get()
        ]);
    }

    public function store()
    {
        return view('kendaraans.store', [
            'kendaraans' => Kendaraans::latest()->get()
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = Kendaraans::find($id);

        return view('kendaraans.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Kendaraans::find($id);
        $validator = Validator::make($request->all(), [
            'brand_kendaraan' => 'required',
            'tipe'            => 'required',
            'name_customer'   => 'required',
            'customer_name'   => 'required',
        ]);

        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

        $created_at = date("Y-m-d H:i:s", strtotime($data->created_at));

        $data['brand_kendaraan'] = $request->brand_kendaraan;
        $data['tipe'] = $request->tipe;
        $data['name_customer'] = $request->name_customer;
        $data['customer_name'] = $request->customer_name;
        
        $data->timestamps = false;
        $data['created_at'] = $created_at;
        $data['updated_at'] = now();

        $dataArray = $data->toArray();

        Kendaraans::whereId($id)->update($dataArray);

        return redirect()->route('kendaraans');
    }

    public function delete(Request $request, $id)
    {
        $data = Kendaraans::find($id);

        if ($data) {
            $data->forceDelete();
        }

        return redirect()->route('kendaraans');
    }
}
