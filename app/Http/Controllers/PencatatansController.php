<?php

namespace App\Http\Controllers;

use App\Models\Balances;
use App\Models\Pencatatans;
use Illuminate\Http\Request;

class PencatatansController extends Controller
{
    public function index()
    {
        return view('pencatatans.index', [
            'pencatatans' => Pencatatans::latest()->get()
        ]);
    }

    public function add()
    {
        return view('pencatatans.insert', [
            'pencatatans' => Pencatatans::latest()->get()
        ]);
    }

    public function store()
    {
        return view('pencatatans.store', [
            'pencatatans' => Pencatatans::latest()->get()
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = Pencatatans::find($id);

        return view('pencatatans.edit', compact('data'));
    }

    public function delete(Request $request, $id)
    {
        $data = Pencatatans::find($id);

        if ($data) {
            $data->forceDelete();
        }

        $dataBalance = Balances::find(1);

        if ($data['income'] == 0 || $data['income'] == null) {
            $dataBalance['balance'] =  $dataBalance['balance'] + $data['outcome'];
        } else {
            $dataBalance['balance'] =  $dataBalance['balance'] - $data['income'];
        }

        $dataBalance->save();

        return redirect()->route('pencatatans');
    }
}
