<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pencatatans extends Model
{
    use HasFactory;

    protected $fillable = ['keterangan', 'created_at', 'in', 'out'];
}
