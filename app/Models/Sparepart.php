<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sparepart extends Model
{
    use HasFactory;

    protected $fillable = ['nama_sparepart', 'brand_kendaraan', 'tipe', 'stok', 'modal', 'jual', 'untung'];
}
