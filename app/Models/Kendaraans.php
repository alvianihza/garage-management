<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kendaraans extends Model
{
    use HasFactory;

    protected $fillable = ['brand_kendaraan', 'tipe', 'name_customer'];
}
