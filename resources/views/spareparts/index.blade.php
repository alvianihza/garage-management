@extends('layout.app')
@section('header')

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sparepart - Bimmers Garage</title>
    <link rel="icon" type="images/gif/png" href="logo.png" />

    <!-- Custom fonts for this template-->
    <link href="{{ asset('template/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('template/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>
<style>
    .cancelbtn,
    .deletebtn {
        float: left;
        width: 50%;
    }

    .cancelbtn {
        background-color: #ccc;
        color: black;
    }

    .deletebtn {
        background-color: #f44336;
    }

    .container {
        padding: 16px;
        text-align: center;
    }

    .modal {
        display: none;
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: #ffffff;
    }

    .modal-content {
        background-color: #fefefe;
        margin: 5% auto 15% auto;
        border: 1px solid #888;
        width: 80%;
    }

    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    .close {
        position: absolute;
        right: 35px;
        top: 15px;
        font-size: 40px;
        font-weight: bold;
        color: #f1f1f1;
    }

    .close:hover,
    .close:focus {
        color: #f44336;
        cursor: pointer;
    }

    .clearfix::after {
        content: "";
        clear: both;
        display: table;
    }

    @media screen and (max-width: 300px) {

        .cancelbtn,
        .deletebtn {
            width: 100%;
        }
    }
</style>

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Sparepart</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('spareparts.insert') }}" class="btn btn-primary btn-sm" id="tambahButton"><i class="fa fa-plus"> Tambah</i></a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Nama Sparepart</th>
                        <th>Brand Kendaraan</th>
                        <th>Tipe</th>
                        <th>Stok</th>
                        <th>Modal (pcs)</th>
                        <th>Jual (pcs)</th>
                        <th>Untung (pcs)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1 ?>
                    @foreach ($spareparts as $row)
                    <tr class="text-center">
                        <td>{{ $no++ }}</td>
                        <td>{{ $row->nama_sparepart }}</td>
                        <td>{{ $row->brand_kendaraan }}</td>
                        <td>{{ $row->tipe }}</td>
                        <td>{{ $row->stok }}</td>
                        <td>{{ $row->modal }}</td>
                        <td>{{ $row->jual }}</td>
                        <td>{{ $row->untung }}</td>
                        <td>
                            <a href="{{ route('spareparts.edit', ['id' => $row->id]) }}" class="btn btn-warning btn-sm" id="editButton"> Edit </a>
                            <button class="btn btn-danger btn-sm" onclick="handleDeleteTable({{ $row->id }});" id="deleteButton"> Delete </i></a>
                        </td>
                    </tr>

                    <div id="id{{ $row->id }}" class="modal" style="position: fixed;top: 0vw; left: 0vw;">
                        <span onclick="document.getElementById('id{{ $row->id }}').style.display='none'" class="close" title="Close Modal">×</span>
                        <div class="container">
                            <h1>Hapus Sparepart</h1>
                            <p>Apakah anda yakin ingin mengapus sparepart <b>{{ $row->nama_sparepart }}</b> ?</p>
                            <form action="{{route('spareparts.delete',['id' =>  $row->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="clearfix">
                                    <button type="button" onclick="document.getElementById('id{{ $row->id }}').style.display='none'" class="cancelbtn">Tidak, Batal</button>
                                    <button type="submit" class="deletebtn">Ya, Hapus</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    function handleDeleteTable(id) {
        document.getElementById(`id${id}`).style.display = 'block';
        modal = id;
    };
</script>

<script>
    <?php
    $conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
    if ($conn->connect_errno) {
        echo "Failed to connect to MySQL: " . $conn->connect_error;
        exit();
    }

    //Retrieve Customer
    $sql = "select * from users";
    $result = ($conn->query($sql));
    $row = [];

    if ($result->num_rows > 0) $row = $result->fetch_all(MYSQLI_ASSOC);
    ?>

    var user = <?php echo json_encode($row); ?>;
    var email = localStorage.getItem("email");
    var role = user.find(obj => obj.email === email).role;
    var tambahButton = document.getElementById("tambahButton");
    var editButton = document.getElementById("editButton");
    var deleteButton = document.getElementById("deleteButton");

    if (role === 'admin') {
        tambahButton.style.display = 'none';
        editButton.textContent = "View";
        deleteButton.style.display = 'none';
    }
</script>

@endsection