<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registration - Bimmers Garage</title>
    <link rel="icon" type="images/gif/png" href="logo.png" />

    <!-- Custom fonts for this template-->
    <link href="{{ asset('template/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('template/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

<body id="page-top">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                            <div class="row">
                                <form action="{{ route('users.store') }}" method="post" id="add_form" style="width: 560px;">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control form-control-user" id="name" name="name" placeholder="Masukkan Nama User" style="font-size: 0.8rem;border-radius: 10rem;padding: 1.5rem 1rem;">
                                            @error('user name')
                                            <div>{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control form-control-user" id="email" name="email" placeholder="Masukkan Email User" cols="50" style="font-size: 0.8rem;border-radius: 10rem;padding: 1.5rem 1rem;">
                                            @error('email')
                                            <div>{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Masukkan Password" cols="50" style="font-size: 0.8rem;border-radius: 10rem;padding: 1.5rem 1rem;">
                                            @error('password')
                                            <div>{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Masukkan Ulang Password" cols="50" style="font-size: 0.8rem;border-radius: 10rem;padding: 1.5rem 1rem;">
                                            @error('password')
                                            <div>{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12" style="display:grid; grid-template-columns: auto auto; gap:10px;">
                                            <a href="login.html" class="btn btn-primary btn-user btn-block" style="display:none;">
                                                Register
                                            </a>
                                            <input type="submit" class="btn btn-primary btn-user btn-block" value="Register" style="border-radius: 10rem;">
                                            <a href="{{ route('login') }}" class="btn btn-outline-primary btn-user btn-block" style="border-radius: 10rem;">
                                                Kembali
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="{{ route('forgetpassword') }}">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small"href="{{ route('login') }}">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('template/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('template/js/sb-admin-2.min.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ asset('template/vendor/chart.js/Chart.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('template/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('template/js/demo/chart-pie-demo.js') }}"></script>

</body>

</html>