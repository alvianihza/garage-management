<?php
$name_customer = $_POST["name_customer"];
$brand_kendaraan = $_POST["brand_kendaraan"];
$tipe = $_POST["tipe"];

// Jenis Pembayaran
$jenis_pembayaran = isset($_POST['inputDP']) ? 'DP' : 'Lunas';
$value_dp = isset($_POST['inputDP']) ? $_POST['inputDP'] : 0;

//Diskon
$diskon = isset($_POST['inputDiskon']) ? $_POST['inputDiskon'] : 0;

//Pengerjaan
$jsonPengerjaan = $_POST['dataFromJS'];
if (isset($_POST['dataFromJS'])) {
    $jsonPengerjaan = $_POST['dataFromJS'];
    $pengerjaan = $jsonPengerjaan;
} else $pengerjaan = "Default Pengerjaan";

//Grand Total Pelunasan
$jsonGrandPelunasan = $_POST['dataGrandPelunasan'];
if ($jsonGrandPelunasan) {
    $grandtotalpelunasan = json_decode($jsonGrandPelunasan, true);
    $grand_total = str_replace(',', '', $grandtotalpelunasan[0]['grand_total']);
    $kekurangan = (isset($grandtotalpelunasan[0]['pelunasan'])) ? str_replace(',', '', $grandtotalpelunasan[0]['pelunasan']) : 0;
} else {
    $grand_total = 0;
    $kekurangan = 0;
}

$stok_reduction = 1;

$conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
if ($conn->connect_error) {
    die('Connection Failed : ' . $conn->connect_error);
} else {
    $stmt = $conn->prepare("INSERT into transaksis( name_customer, brand_kendaraan, tipe, jenis_pembayaran, value_dp, diskon, pengerjaan, grand_total, kekurangan, created_at) 
                            VALUES(?, ?, ?, ?, ?, ? ,?, ?, ?, NOW())");
    $stmt->bind_param("ssssissii", $name_customer, $brand_kendaraan, $tipe, $jenis_pembayaran, $value_dp, $diskon, $pengerjaan, $grand_total, $kekurangan);
    $stmt->execute();
    $stmt->close();

    if ($value_dp === 0) {
        $keterangan = 'Down Payment Pengerjaan Mobil ' . $name_customer . ' ( ' . $brand_kendaraan . $tipe . ' ) ';
        $income = $grand_total;
    } else {
        $keterangan = 'Pengerjaan Mobil ' . $name_customer . ' ( ' . $brand_kendaraan . $tipe . ' ) ';
        $income = $value_dp;
    };

    $second = $conn->prepare("INSERT into pencatatans( keterangan, income, created_at) 
                            VALUES(?, ?, NOW())");
    $second->bind_param("si", $keterangan, $income);
    $second->execute();
    $second->close();
    // $conn->close();
}

// Mendekode data pengerjaan dari format JSON
$data_pengerjaan = json_decode($pengerjaan, true);

// Menyiapkan variabel untuk menyimpan data sparepart
$nama_sparepart = "";
$kuantitas = 0;

// Melakukan looping melalui setiap entri data pengerjaan
foreach ($data_pengerjaan as $item) {
    // Memeriksa jika jenis pengerjaan adalah "sparepart"
    if ($item['jenispengerjaan'] === 'sparepart') {
        // Menyimpan nama sparepart dan kuantitas
        $nama_sparepart = $item['namasparepart'];
        $kuantitas = $item['kuantitas'];

        // Disini Anda dapat menyimpan data tersebut ke dalam tabel atau melakukan operasi lain sesuai kebutuhan
        // Misalnya:
        // $stmt_spareparts->bind_param("si", $nama_sparepart, $kuantitas);
        // $stmt_spareparts->execute();
        $stmt_update_stok = $conn->prepare("UPDATE spareparts SET stok = stok - ? WHERE nama_sparepart = ?");
        $stmt_update_stok->bind_param("is", $kuantitas, $nama_sparepart);

        // Mengeksekusi pernyataan SQL untuk memperbarui nilai stok dalam tabel "spareparts"
        $stmt_update_stok->execute();
        $stmt_update_stok->close();

        // Melakukan commit transaksi karena semua pernyataan SQL berhasil dieksekusi
        $conn->commit();
    }
}
?>


<!DOCTYPE html>
<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">
</head>
<style>
    body {
        text-align: center;
        padding: 40px 0;
        background: #EBF0F5;
    }

    h1 {
        color: #88B04B;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-weight: 900;
        font-size: 40px;
        margin-bottom: 10px;
    }

    p {
        color: #404F5E;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-size: 20px;
        margin: 0;
    }

    i {
        color: #9ABC66;
        font-size: 100px;
        line-height: 200px;
        margin-left: -15px;
    }

    .card {
        background: white;
        padding: 60px;
        border-radius: 4px;
        box-shadow: 0 2px 3px #C8D0D8;
        display: inline-block;
        margin: 0 auto;
    }

    .btn {
        display: inline-block;
        font-weight: 400;
        color: #858796;
        text-align: center;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-color: transparent;
        border: 1px solid transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 0.35rem;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    .btn-outline-primary {
        color: #4e73df;
        border-color: #4e73df;
    }

    .btn-outline-primary:hover {
        color: #fff;
        background-color: #4e73df;
        border-color: #4e73df;
    }

    .btn-outline-primary:focus,
    .btn-outline-primary.focus {
        box-shadow: 0 0 0 0.2rem rgba(78, 115, 223, 0.5);
    }

    .btn-outline-primary.disabled,
    .btn-outline-primary:disabled {
        color: #4e73df;
        background-color: transparent;
    }

    .btn-outline-primary:not(:disabled):not(.disabled):active,
    .btn-outline-primary:not(:disabled):not(.disabled).active,
    .show>.btn-outline-primary.dropdown-toggle {
        color: #fff;
        background-color: #4e73df;
        border-color: #4e73df;
    }

    .btn-outline-primary:not(:disabled):not(.disabled):active:focus,
    .btn-outline-primary:not(:disabled):not(.disabled).active:focus,
    .show>.btn-outline-primary.dropdown-toggle:focus {
        box-shadow: 0 0 0 0.2rem rgba(78, 115, 223, 0.5);
    }
</style>

<body>
    <div class="card">
        <div style="border-radius:200px; height:200px; width:200px; background: #F8FAF5; margin:0 auto;">
            <i class="checkmark">✓</i>
        </div>
        <h1>Sukses</h1>
        <p>Transaksi baru berhasil ditambahkan</p>
        <a href="/transaksi" class="btn btn-outline-primary" style="margin-top: 20px;text-decoration:none">
            Selesai
        </a>
    </div>
    <script>
        var token = localStorage.getItem("token");
        if (!token || token === undefined || token === null) {
            window.location.href = "{{ route('login') }}";
        };
    </script>
    <script>
    </script>
</body>

</html>