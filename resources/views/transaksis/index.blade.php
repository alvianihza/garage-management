@extends('layout.app')
@section('header')

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Transaksi - Bimmers Garage</title>
    <link rel="icon" type="images/gif/png" href="logo.png" />

    <!-- Custom fonts for this template-->
    <link href="{{ asset('template/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('template/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Transaksi</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <a href="{{ route('transaksis.insert') }}" class="btn btn-primary btn-sm" id="tambahButton"><i class="fa fa-plus"> Tambah</i></a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Customer</th>
                        <th>Brand Kendaraan</th>
                        <th>Tipe</th>
                        <th>Grand Total</th>
                        <th>Kekurangan</th>
                        <th>Tanggal Pengerjaan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1 ?>
                    @foreach ($transaksis as $row)
                    <tr class="text-center">
                        <td>{{ $no++ }}</td>
                        <td>{{ $row->name_customer }}</td>
                        <td>{{ $row->brand_kendaraan }}</td>
                        <td>{{ $row->tipe }}</td>
                        <td>{{ number_format($row->grand_total, 0, ',', ',') }}</td>
                        <td>{{ number_format($row->kekurangan, 0, ',', ',') }}</td>
                        <td>{{ $row->tanggal_pengerjaan }}</td>
                        <td>
                            @if($row->kekurangan == 0) <a href="{{ route('transaksis.view', ['id' => $row->id]) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"> View </i></a>
                            @else <a href="{{ route('transaksis.edit', ['id' => $row->id]) }}" class="btn btn-warning btn-sm" id="editButton"><i class="fas fa-edit"> Edit</i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    <?php
    $conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
    if ($conn->connect_errno) {
        echo "Failed to connect to MySQL: " . $conn->connect_error;
        exit();
    }

    //Retrieve Customer
    $sql = "select * from users";
    $result = ($conn->query($sql));
    $row = [];

    if ($result->num_rows > 0) $row = $result->fetch_all(MYSQLI_ASSOC);
    ?>

    var user = <?php echo json_encode($row); ?>;
    var email = localStorage.getItem("email");
    var role = user.find(obj => obj.email === email).role;
    var tambahButton = document.getElementById("tambahButton");
    var editButton = document.getElementById("editButton");

    if (role === 'admin') {
        tambahButton.style.display = 'none';
        editButton.style.display = 'none';
    }
</script>

@endsection