<?php
//Connection DB
$conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: " . $conn->connect_error;
    exit();
}

//Retrieve Customer
$sql = "select * from customers";
$result = ($conn->query($sql));
$row = [];

if ($result->num_rows > 0) $row = $result->fetch_all(MYSQLI_ASSOC);

if (isset($row[0]['nama'])) {
    $nama = $row[0]['nama'];
} else $nama = '-';

//Retrieve Kendaraan
$sqlKendaraan = "select * from kendaraans";
$resultKendaraan = ($conn->query($sqlKendaraan));
$rowKendaraan = [];

if ($resultKendaraan->num_rows > 0) $rowKendaraan = $resultKendaraan->fetch_all(MYSQLI_ASSOC);

//Data Enum Brand Kendaraan
$brandList = array();
foreach ($rowKendaraan as $car) {
    $brand = $car['brand_kendaraan'];
    if (!in_array($brand, $brandList)) $brandList[] = $brand;
};
if (isset($rowKendaraan[0]['brand_kendaraan'])) {
    $brandKendaraan = $rowKendaraan[0]['brand_kendaraan'];
} else $brandKendaraan = '-';


//Data Enum Tipe
$tipeList = array();
foreach ($rowKendaraan as $car) {
    $brand = $car['brand_kendaraan'];
    $tipe = $car['tipe'];
    if (!array_key_exists($brand, $tipeList)) $tipeList[$brand] = array();
    $tipeList[$brand][] = $tipe;
};
if (isset($rowKendaraan[0]['tipe'])) {
    $tipeKendaraan = $rowKendaraan[0]['tipe'];
} else $tipeKendaraan = '-';

$optionsByBrandJSON = json_encode($tipeList);

//Retrieve Sparepart
$sql = "select * from spareparts";
$resultSparepart = ($conn->query($sql));
$rowSparepart = [];

if ($resultSparepart->num_rows > 0) $rowSparepart = $resultSparepart->fetch_all(MYSQLI_ASSOC);

$sparepartsList = array();
foreach ($rowSparepart as $parts) {
    $brand = $parts['nama_sparepart'];
    if (!in_array($brand, $sparepartsList)) {
        $sparepartsList[] = $brand;
    }
};

?>

<!DOCTYPE html>
<html lang="en">

<style>
    .hr.rounded {
        border-top: 8px solid #bbb;
        border-radius: 5px;
    }

    .toggle {
        --width: 60px;
        --height: calc(var(--width) / 2);

        position: relative;
        display: inline-block;
        width: var(--width);
        height: var(--height);
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
        border-radius: var(--height);
        cursor: pointer;
    }

    .toggle input {
        display: none;
    }

    .toggle .slider {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border-radius: var(--height);
        background-color: #ccc;
        transition: all 0.4s ease-in-out;
    }

    .toggle .slider::before {
        content: '';
        position: absolute;
        top: 10%;
        left: 10%;
        width: calc(var(--height)*0.8);
        height: calc(var(--height)*0.8);
        border-radius: calc(var(--height) / 2);
        background-color: #fff;
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
        transition: all 0.4s ease-in-out;
    }

    .toggle input:checked+.slider {
        background-color: #2196F3;
    }

    .toggle input:checked+.slider::before {
        transform: translateX(calc(var(--width)*1.05 - var(--height)));
    }

    .toggle .labels {
        position: absolute;
        top: 6px;
        width: 100%;
        height: 100%;
        font-size: 12px;
        font-family: sans-serif;
        transition: all 0.4s ease-in-out;
    }

    .toggle .labels::after {
        content: attr(data-off);
        position: absolute;
        right: 15px;
        color: #4d4d4d;
        opacity: 1;
        text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.4);
        transition: all 0.4s ease-in-out;
    }

    .toggle .labels::before {
        content: attr(data-on);
        position: absolute;
        left: 15px;
        color: #ffffff;
        opacity: 0;
        text-shadow: 1px 1px 2px rgba(255, 255, 255, 0.4);
        transition: all 0.4s ease-in-out;
    }

    .toggle input:checked~.labels::after {
        opacity: 0;
    }

    .toggle input:checked~.labels::before {
        opacity: 1;
    }
</style>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Transaksi - Bimmers Garage</title>
    <link rel="icon" type="images/gif/png" href="../../logo.png" />

    <!-- Custom fonts for this template-->
    <link href="{{ asset('template/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('template/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar" style="position:fixed;top:0;bottom:0;">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-logo mx-3"><img src="../../logo.png" alt="logo" width="30" /></div>
                <div class="sidebar-brand-text mx-3"><img src="../../logo.jpeg" alt="logo" width="200" style="border-radius:10px;margin-top:5vw" /></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item" style="margin-top:5vw">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <hr class="sidebar-divider">

            <!-- Nav Item - Transaksi -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('transaksis') }}">
                    <i class="fas fa-money-bill"></i>
                    <span>Transaksi</span></a>
            </li>

            <!-- Nav Item - Transaksi -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pencatatan -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('pencatatans') }}">
                    <i class="fa fa-book"></i>
                    <span>Pencatatan</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('customers') }}">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Customers</span></a>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('kendaraans') }}">
                    <i class="fa fa-car"></i>
                    <span>Kendaraan</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('spareparts') }}">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Sparepart</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('users') }}">
                    <i class="fas fa-user"></i>
                    <span>User</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle" onclick="sidebarButton()"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column" style="position:relative;margin-left:17vw;">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small" id="userProfile">Mas Iwan</span>
                                <img class="img-profile rounded-circle" src="../../profile.png" style="margin-left:12px">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- End of Topbar -->
                <div class="container-fluid">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Transaksi</h1>
                    </div>

                    <form action="{{ route('transaksis.store') }}" class="transaksi" method="post" id="add_form">
                        @csrf
                        <div style="display:grid;grid-template-columns: auto auto; gap:25px;">
                            <div class="col" style="margin-left: 1rem;width: 75%">
                                <div style="width:60%">
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Nama Customer:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <select name="name_customer" class="form-control form-control-user" id="name_customer" placeholder="Pilih Nama Customer" onchange="handleCustomer()">
                                                @foreach ($row as $data)
                                                <option value="{{ $data['nama'] }}">{{ $data['nama'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Brand Kendaraan:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <select name="brand_kendaraan" class="form-control form-control-user" id="brand_kendaraan" placeholder="Pilih Brand Kendaraan" onchange="handleBrandKendaraan()">
                                                @foreach ($brandList as $data)
                                                <option value="{{ $data }}">{{ $data }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Tipe:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <select name="tipe" class="form-control form-control-user" id="tipe" placeholder="Pilih Tipe Kendaraan" onchange="handleTipe()"> </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Tanggal Pengerjaan:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="datetime-local" class="form-control form-control-user" id="inputDate" onchange="handleDate()">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Jenis Pembayaran:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <row>
                                                <col>
                                                <input type="radio" id="lunas" name="jenis-pembayaran" value="lunas" onchange="checkOption()">
                                                <label for="lunas">Lunas</label>
                                                <col />
                                                <col>
                                                <input type="radio" id="dp" name="jenis-pembayaran" value="dp" style="margin-left:20px;" onchange="checkOption()">
                                                <label for="dp">DP</label>
                                                <col />
                                            </row>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;display:none" id="jumlahDP">
                                            <label for="fname">Jumlah DP:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control form-control-user" id="inputDP" name="inputDP" style="display:none" placeholder="Masukkan Jumlah DP" onchange="handlePekerjaan()">
                                        </div>
                                    </div>

                                    <div class="form-group row" id="diskon" style="margin-top:-20px">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Diskon:</label><br>
                                        </div>
                                        <div class="col-sm-12" style="display:grid;grid-template-columns: auto auto; gap:10px;">
                                            <label class="toggle" style="width: 70px;">
                                                <input type="checkbox" id="useDiskon" onchange="checkDiskon()">
                                                <span class="slider"></span>
                                                <span class="labels" data-on="Yes" data-off="No"></span>
                                            </label>
                                            <input type="text" class="form-control form-control-user" name="inputDiskon" id="inputDiskon" disabled onchange="handlePekerjaan()">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div id="show_item">
                                        <div class="card">
                                            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                                <h6 class="m-0 font-weight-bold text-primary">Pengerjaan</h6>
                                            </div>
                                            <div class="card-body" style="margin-right: 14rem;">
                                                <div class="form-group row">
                                                    <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                                        <label for="fname">Jenis Pengerjaan:</label><br>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select name="inputJenisPengerjaan" class="form-control form-control-user" id="inputJenisPengerjaan" placeholder="Pilih Jenis" onchange="handlePekerjaan()">
                                                            <option value="jasa">Jasa</option>
                                                            <option value="sparepart">Sparepart</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row" id="containerPengerjaan" style="display:''">
                                                    <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end; display:''" id="namaPengerjaan">
                                                        <label name="product_name[]" for="fname">Nama Pengerjaan:</label><br>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input name="inputNamaPengerjaan" type="text" class="form-control form-control-user" id="inputNamaPengerjaan" placeholder="Masukkan Nama Pengerjaan" onchange="handlePekerjaan()" style="display:''">
                                                    </div>
                                                </div>
                                                <div class="form-group row" id="containerSparepart">
                                                    <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="namaSparepart">
                                                        <label for="fname">Nama Sparepart:</label><br>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <select name="inputNamaSparepart" class="form-control form-control-user inputNamaSparepart" id="inputNamaSparepart" placeholder="Pilih Sparepart" onchange="handleSparepartChange()">
                                                            <option value="-">-</option>
                                                            @foreach ($sparepartsList as $data)
                                                            <option value="{{ $data }}">{{ $data }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="jumlahDP">
                                                        <label name="product_name[]" for="fname">Harga:</label><br>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input name="inputHarga" type="text" class="form-control form-control-user" id="inputHarga" placeholder="Masukkan Harga" onchange="handlePekerjaan()">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="jumlahDP">
                                                        <label name="product_name[]" for="fname">Kuantitas:</label><br>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input name="inputKuantitas" type="number" class="form-control form-control-user" id="inputKuantitas" placeholder="Masukkan Kuantitas" onchange="handlePekerjaan()">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding: 0px 0px 20px 0px;">
                                        <button class="btn btn-outline-primary btn-user btn-block add_item_btn" style="position:relative;margin-top:15px;border: 1px dashed;margin-bottom:7vw">
                                            + Tambah Pengerjaan Lainnya
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <div>
                                <div style="position:absolute;overflow:hidden;width: 24rem; margin-left: -12rem;height:auto;">
                                    <div class="card shadow mb-4">
                                        <div class="card-body">
                                            <div style="text-align: center;"><strong>- Detail Nota -</strong></div>
                                            <div style="text-align: center;margin-top: 2vw">
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Customer</strong></p>
                                                    <div style="text-align: end;" id="outputCustomer">{{$nama}}</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Brand Kendaraan</strong></p>
                                                    <div style="text-align: end;" id="outputBrandKendaraan">{{$brandKendaraan}}</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Tipe</strong></p>
                                                    <div style="text-align: end;" id="outputType">{{$tipeKendaraan}}</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Tanggal Pengerjaan</strong></p>
                                                    <div style="text-align: end;" id="outputDate">-</div>
                                                </div>
                                                <hr class="rounded" style="margin-top:-0.2vw">
                                                <div style="display:grid;grid-template-columns: auto auto;margin-top:-1.5vw">
                                                    <p style="text-align: start;margin-top:20px"><strong>Jasa</strong></p>
                                                </div>
                                                <div id="outputJasa">
                                                    <div style="text-align: end;">-</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;margin-top:20px"><strong>Sparepart</strong></p>
                                                </div>
                                                <div id="outputSparepart">
                                                    <div style="text-align: end;">-</div>
                                                </div>
                                                <hr class="rounded">
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Total</strong></p>
                                                    <div style="text-align: end;" id="outputTotal">-</div>
                                                </div>
                                                <div style="display:none;grid-template-columns: auto auto;" id="outputDiskon">
                                                    <p style="text-align: start;"><strong>Diskon</strong></p>
                                                    <div style="text-align: end;" id="outputValueDiskon">-</div>
                                                </div>
                                                <hr class="rounded">
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Grand Total</strong></p>
                                                    <div style="text-align: end;" id="outputGrandTotal">-</div>
                                                </div>
                                                <div style="display:none;grid-template-columns: auto auto;" id="outputDP">
                                                    <p style="text-align: start;"><strong>Down Payment</strong></p>
                                                    <div style="text-align: end;" id="outputValueDP">-</div>
                                                </div>
                                                <hr class="rounded">
                                                <div style="display:none;grid-template-columns: auto auto;" id="outputPelunasan">
                                                    <p style="text-align: start;"><strong>Pelunasan / Kekurangan</strong></p>
                                                    <div style="text-align: end;" id="outputValuePelunasan">-</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="dataFromJS" id="dataFromJS" style="display: none;">
                                    <input type="hidden" name="dataGrandPelunasan" id="dataGrandPelunasan" style="display: none;">

                                    <input type="submit" class="btn btn-primary btn-user btn-block" value="Buat Transaksi">
                                    <a href="{{ route('transaksis') }}" class="btn btn-outline-primary btn-user btn-block">
                                        Kembali
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span><strong>Star Oto Service Management </strong> &copy; 2024 Created by Alvian Andro</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->
        </div>
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="{{ route('login') }}" onclick="handleLogout()">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js'></script>
    
    <script>
        <?php
        $conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
        if ($conn->connect_errno) {
            echo "Failed to connect to MySQL: " . $conn->connect_error;
            exit();
        }

        //Retrieve Customer
        $sql = "select * from users";
        $result = ($conn->query($sql));
        $row = [];

        if ($result->num_rows > 0) $row = $result->fetch_all(MYSQLI_ASSOC);
        ?>

        var user = <?php echo json_encode($row); ?>;
        var email = localStorage.getItem("email");
        var username = user.find(obj => obj.email === email).name;
        var userProfile = document.getElementById("userProfile");

        userProfile.textContent = username;

        var token = localStorage.getItem("token");
        if (!token || token === undefined || token === null) {
            window.location.href = "{{ route('login') }}";
        };

        function handleLogout() {
            localStorage.clear();
        };
    </script>

    <!-- Enum Tipe -->
    <script>
        var brandDropdown = document.getElementById('brand_kendaraan');
        var tipeDropdown = document.getElementById('tipe');
        var optionsByBrand = <?= $optionsByBrandJSON ?>;

        function updateTipeOptions() {
            var selectedBrand = brandDropdown.value;
            tipeDropdown.innerHTML = '';
            if (selectedBrand && optionsByBrand[selectedBrand]) {
                optionsByBrand[selectedBrand].forEach(function(option) {
                    var optionElement = document.createElement('option');
                    optionElement.value = option;
                    optionElement.textContent = option;
                    tipeDropdown.appendChild(optionElement);
                });
            }
        }
        brandDropdown.addEventListener('change', updateTipeOptions);
        updateTipeOptions();
    </script>

    <script>
        function checkOption() {
            //This will fire on changing of the value of "requests"
            var lunas = document.getElementById("lunas");
            var dp = document.getElementById("dp");

            var jumlahDP = document.getElementById("jumlahDP");
            var inputDP = document.getElementById("inputDP");
            var diskon = document.getElementById("diskon");

            var outputDP = document.getElementById("outputDP");

            if (dp.checked) {
                diskon.style.marginTop = '0px'
                jumlahDP.style.display = 'inline'
                inputDP.style.display = 'inline'
                outputDP.style.display = 'grid'
                outputPelunasan.style.display = 'grid'
            } else {
                diskon.style.marginTop = '-20px'
                jumlahDP.style.display = 'none'
                inputDP.style.display = 'none'
                outputDP.style.display = 'none'
                outputPelunasan.style.display = 'none'
            }
        };

        function checkDiskon() {
            var useDiskon = document.getElementById("useDiskon");
            var inputDiskon = document.getElementById("inputDiskon");

            var outputDiskon = document.getElementById("outputDiskon");

            if (useDiskon.checked) {
                inputDiskon.removeAttribute("disabled");
                outputDiskon.style.display = 'grid'
            } else {
                inputDiskon.setAttribute("disabled", "disabled");
                outputDiskon.style.display = 'none'
            }
        };

        document.addEventListener('DOMContentLoaded', function() {
            document.querySelector('.add_item_btn').addEventListener('click', function(e) {
                e.preventDefault();

                const div = document.createElement('div');
                div.innerHTML = `
                    <div class="card" style="margin-top:10px">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Pengerjaan</h6>
                            <button class="btn btn-danger remove_item_btn" style="border: none;border-radius:10px;padding: 2px 18px 2px 10px">
                                <i class="fas fa-times-circle fa-sm fa-fw text-white"></i>
                                Hapus Pengerjaan
                            </button>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body" style="margin-right: 14rem;">
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                    <label for="fname">Jenis Pengerjaan:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <select name="inputJenisPengerjaan" class="form-control form-control-user" id="inputJenisPengerjaan" placeholder="Pilih Type" onchange="handlePekerjaan()">
                                        <option value="jasa">Jasa</option>
                                        <option value="sparepart">Sparepart</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="containerPengerjaan" style="display:''">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="namaPengerjaan" style="display:''">
                                    <label name="product_name[]" for="fname">Nama Pengerjaan:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <input name="inputNamaPengerjaan" type="text" class="form-control form-control-user" id="inputNamaPengerjaan" placeholder="Masukkan Nama Pengerjaan" onchange="handlePekerjaan()" style="display:''">
                                </div>
                            </div>
                            <div class="form-group row" id="containerSparepart">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end; id="namaSparepart ">
                                    <label for="fname">Nama Sparepart:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <select name="inputNamaSparepart" class="form-control form-control-user inputNamaSparepart" id="inputNamaSparepart" placeholder="Pilih Sparepart" onchange="handleSparepartChange()">
                                        <option value="-">-</option>    
                                        @foreach ($sparepartsList as $data)
                                        <option value="{{ $data }}">{{ $data }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="jumlahDP">
                                    <label name="product_name[]" for="fname">Harga:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <input name="inputHarga" type="text" class="form-control form-control-user" id="inputHarga" placeholder="Masukkan Harga" onchange="handlePekerjaan()">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="jumlahDP">
                                    <label name="product_name[]" for="fname">Kuantitas:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <input name="inputKuantitas" type="number" class="form-control form-control-user" id="inputKuantitas" placeholder="Masukkan Kuantitas" onchange="handlePekerjaan()">
                                </div>
                            </div>
                        </div>
                    </div>
                `;
                document.querySelector('#show_item').append(div);
            });
        });

        document.addEventListener('click', function(e) {
            if (e.target.matches('.remove_item_btn')) {
                e.preventDefault();
                let row_item = e.target.parentElement.parentElement;
                row_item.remove();
                handlePekerjaan();
            }
        });

        function sidebarButton() {
            var siderToggeled = localStorage.getItem("siderToggeled") ? JSON.parse(localStorage.getItem("siderToggeled")) : true;
            var contentWrapper = document.getElementById("content-wrapper");

            if (siderToggeled) {
                localStorage.setItem("siderToggeled", false);
                contentWrapper.style.marginLeft = "7.8vw";
                contentWrapper.style.width = "93vw";
            } else {
                localStorage.setItem("siderToggeled", true);
                contentWrapper.style.marginLeft = "17vw";
                contentWrapper.style.width = "84vw";
            }
        };

        function handleCustomer() {
            var customer = document.getElementById("name_customer").value;
            var outputCustomer = document.getElementById("outputCustomer");

            outputCustomer.innerHTML = customer;
        };

        function handleBrandKendaraan() {
            var brandKendaraan = document.getElementById("brand_kendaraan").value;
            var outputBrandKendaraan = document.getElementById("outputBrandKendaraan");

            outputBrandKendaraan.innerHTML = brandKendaraan;
        };

        function handleTipe() {
            var type = document.getElementById("tipe").value;
            var outputType = document.getElementById("outputType");

            outputType.innerHTML = type;
        };

        function handleDate() {
            var inputDate = new Date(document.getElementById('inputDate').value);
            var outputDate = document.getElementById('outputDate');

            const year = inputDate.getFullYear();
            const month = String(inputDate.getMonth() + 1).padStart(2, '0');
            const day = String(inputDate.getDate()).padStart(2, '0');
            const hours = (inputDate.getHours() > 9) ? String(inputDate.getHours()) : `0${String(inputDate.getHours())}`;
            const minute = (inputDate.getMinutes() > 9) ? String(inputDate.getMinutes()) : `0${String(inputDate.getMinutes())}`;
            const formattedDate = `${day}-${month}-${year}&nbsp;(${hours}:${minute})`;
            outputDate.innerHTML = formattedDate;
        };

        const inputDP = document.getElementById('inputDP');
        const outputValueDP = document.getElementById('outputValueDP');

        inputDP.addEventListener('keyup', function() {
            outputValueDP.textContent = 'Rp. ' + number_format(this.value).toLocaleString("id-ID");
        });

        const inputDiskon = document.getElementById('inputDiskon');
        const outputValueDiskon = document.getElementById('outputValueDiskon');

        inputDiskon.addEventListener('keyup', function() {
            outputValueDiskon.textContent = 'Rp. ' + number_format(this.value).toLocaleString("id-ID");
        });

        var sparepart = [];
        var harga = [];

        function handleSparepartChange() {
            var sparepartList = <?php echo json_encode($rowSparepart); ?>;
            var inputNamaSparepart = document.querySelectorAll(".inputNamaSparepart");
            var inputHarga = document.getElementsByName("inputHarga");

            sparepart = [];
            inputNamaSparepart.forEach(function(value, key) {
                sparepart.push(value.value);
                if (value.value === '-') {
                    inputHarga[key].value = (inputHarga[key].value) ? inputHarga[key].value : null;
                } else inputHarga[key].value = sparepartList.find((obj) => obj.nama_sparepart === value.value).jual;
            });

            this.handlePekerjaan();
        };

        function handlePekerjaan() {
            const inputJenisPengerjaan = document.getElementsByName("inputJenisPengerjaan");
            const inputNamaPengerjaan = document.getElementsByName("inputNamaPengerjaan");
            const inputHarga = document.getElementsByName("inputHarga");
            const inputKuantitas = document.getElementsByName("inputKuantitas");

            const outputTotal = document.getElementById("outputTotal");
            const outputGrandTotal = document.getElementById("outputGrandTotal");
            const outputPelunasan = document.getElementById("outputValuePelunasan");

            let Total = [];
            let GrandTotal = [];
            let Pelunasan = [];

            const outputJasa = document.getElementById("outputJasa");
            const namaPengerjaan = document.getElementById("namaPengerjaan");
            const containerPengerjaan = document.getElementById("containerPengerjaan");
            const namaSparepart = document.getElementById("namaSparepart");
            const containerSparepart = document.getElementById("containerSparepart");

            let pengerjaan = [];
            inputJenisPengerjaan.forEach(function(obj, key) {
                pengerjaan.push({
                    jenispengerjaan: obj.value,
                    namapengerjaan: inputNamaPengerjaan[key].value,
                    namasparepart: (sparepart.length === 0) ? '-' : sparepart[key],
                    harga: inputHarga[key].value,
                    kuantitas: inputKuantitas[key].value
                })
            });

            var jsonPengerjaan = JSON.stringify(pengerjaan);
            document.getElementById('dataFromJS').value = jsonPengerjaan;

            document.querySelector('#outputJasa').innerHTML = "";
            document.querySelector('#outputSparepart').innerHTML = "";

            inputJenisPengerjaan.forEach(function(obj, key) {
                if (obj.value === 'jasa') {
                    const div = document.createElement('div');

                    div.innerHTML = `
                        <div style="display:grid;grid-template-columns: auto auto;">
                            <div style="text-align: start;">${inputNamaPengerjaan[key].value} x ${inputKuantitas[key].value}</div>
                            <div style="text-align: end;">Rp. ${number_format(Number(inputHarga[key].value)*Number(inputKuantitas[key].value)).toLocaleString("id-ID")}</div>
                            <div style="text-align: start;">&nbsp;&nbsp;@ Rp. ${number_format(inputHarga[key].value).toLocaleString("id-ID")}</div>
                        </div>
                    `;
                    document.querySelector('#outputJasa').append(div);
                    Total.push(Number(inputHarga[key].value) * Number(inputKuantitas[key].value));
                } else if (obj.value === 'sparepart') {
                    const div = document.createElement('div');

                    div.innerHTML = `
                        <div style="display:grid;grid-template-columns: auto auto;">
                            <div style="text-align: start;">${(sparepart.length === 0) ? '-' : sparepart[key]} x ${inputKuantitas[key].value}</div>
                            <div style="text-align: end;">Rp. ${number_format(Number(inputHarga[key].value)*Number(inputKuantitas[key].value)).toLocaleString("id-ID")}</div>
                            <div style="text-align: start;">&nbsp;&nbsp;@ Rp. ${number_format(inputHarga[key].value).toLocaleString("id-ID")}</div>
                        </div>
                    `;
                    document.querySelector('#outputSparepart').append(div);
                    Total.push(Number(inputHarga[key].value) * Number(inputKuantitas[key].value));
                }
            });

            if (Total.length !== 0 && Total[0] !== 0) {
                outputTotal.textContent = 'Rp. ' + number_format(Total.reduce(function(acc, curr) {
                    return acc + parseInt(curr, 10);
                }, 0)).toLocaleString("id-ID");

                GrandTotal = Total;
                if (inputDiskon.value) GrandTotal.push(-Number(inputDiskon.value));

                valueGrandTotal = number_format(GrandTotal.reduce(function(acc, curr) {
                    return acc + parseInt(curr, 10);
                }, 0)).toLocaleString("id-ID");
                outputGrandTotal.textContent = 'Rp. ' + valueGrandTotal;

                Pelunasan = GrandTotal;
                if (inputDP.value) Pelunasan.push(-Number(inputDP.value));

                valuePelunasan = number_format(Pelunasan.reduce(function(acc, curr) {
                    return acc + parseInt(curr, 10);
                }, 0)).toLocaleString("id-ID");
                outputPelunasan.textContent = 'Rp. ' + valuePelunasan;

                var jsonGrandTotalPelunasan = JSON.stringify((inputDP.value) ? [{
                    grand_total: valueGrandTotal,
                    pelunasan: valuePelunasan
                }] : [{
                    grand_total: valueGrandTotal,
                }]);
                document.getElementById('dataGrandPelunasan').value = jsonGrandTotalPelunasan;

            };
        };
    </script>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('template/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('template/js/sb-admin-2.min.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ asset('template/vendor/chart.js/Chart.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('template/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('template/js/demo/chart-pie-demo.js') }}"></script>

</body>

</html>