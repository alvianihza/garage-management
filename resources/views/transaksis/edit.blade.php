<?php
$currentUrl = $_SERVER['REQUEST_URI'];
$urlParts = parse_url($currentUrl);

$path = $urlParts['path'];
$pathSegments = explode('/', $path);

$id = end($pathSegments);

$conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: " . $conn->connect_error;
    exit();
}

$sql = "select * from transaksis where id='$id'";
$result = ($conn->query($sql));

$row = [];

if ($result->num_rows > 0) {
    $row = $result->fetch_all(MYSQLI_ASSOC);
}
$dp = ($data->jenis_pembayaran) === 'DP' ? true : false;
$lunas = ($data->jenis_pembayaran) === 'Lunas' ? true : false;
?>

<!DOCTYPE html>
<html lang="en">

<style>
    .hr.rounded {
        border-top: 8px solid #bbb;
        border-radius: 5px;
    }

    .toggle {
        --width: 60px;
        --height: calc(var(--width) / 2);

        position: relative;
        display: inline-block;
        width: var(--width);
        height: var(--height);
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
        border-radius: var(--height);
        cursor: pointer;
    }

    .toggle input {
        display: none;
    }

    .toggle .slider {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border-radius: var(--height);
        background-color: #ccc;
        transition: all 0.4s ease-in-out;
    }

    .toggle .slider::before {
        content: '';
        position: absolute;
        top: 10%;
        left: 10%;
        width: calc(var(--height)*0.8);
        height: calc(var(--height)*0.8);
        border-radius: calc(var(--height) / 2);
        background-color: #fff;
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3);
        transition: all 0.4s ease-in-out;
    }

    .toggle input:checked+.slider {
        background-color: #2196F3;
    }

    .toggle input:checked+.slider::before {
        transform: translateX(calc(var(--width)*1.05 - var(--height)));
    }

    .toggle .labels {
        position: absolute;
        top: 6px;
        width: 100%;
        height: 100%;
        font-size: 12px;
        font-family: sans-serif;
        transition: all 0.4s ease-in-out;
    }

    .toggle .labels::after {
        content: attr(data-off);
        position: absolute;
        right: 15px;
        color: #4d4d4d;
        opacity: 1;
        text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.4);
        transition: all 0.4s ease-in-out;
    }

    .toggle .labels::before {
        content: attr(data-on);
        position: absolute;
        left: 15px;
        color: #ffffff;
        opacity: 0;
        text-shadow: 1px 1px 2px rgba(255, 255, 255, 0.4);
        transition: all 0.4s ease-in-out;
    }

    .toggle input:checked~.labels::after {
        opacity: 0;
    }

    .toggle input:checked~.labels::before {
        opacity: 1;
    }
</style>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Transaksi - Star Oto Service</title>
    <link rel="icon" type="images/gif/png" href="../../logo.png" />

    <!-- Custom fonts for this template-->
    <link href="{{ asset('template/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('template/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar" style="position:fixed;top:0;bottom:0;">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-logo mx-3"><img src="../../logo.png" alt="logo" width="30" /></div>
                <div class="sidebar-brand-text mx-3"><img src="../../logo.jpeg" alt="logo" width="200" style="border-radius:10px;margin-top:5vw" /></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item" style="margin-top:5vw">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <hr class="sidebar-divider">

            <!-- Nav Item - Transaksi -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('transaksis') }}">
                    <i class="fas fa-money-bill"></i>
                    <span>Transaksi</span></a>
            </li>

            <!-- Nav Item - Transaksi -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pencatatan -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('pencatatans') }}">
                    <i class="fa fa-book"></i>
                    <span>Pencatatan</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('customers') }}">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Customers</span></a>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('kendaraans') }}">
                    <i class="fa fa-car"></i>
                    <span>Kendaraan</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('spareparts') }}">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Sparepart</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('users') }}">
                    <i class="fas fa-user"></i>
                    <span>User</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle" onclick="sidebarButton()"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column" style="position:relative;margin-left:17vw;">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small" id="userProfile">Mas Iwan</span>
                                <img class="img-profile rounded-circle" src="../../profile.png" style="margin-left:12px">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- End of Topbar -->
                <div class="container-fluid">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Edit Transaksi</h1>
                    </div>

                    <form action="{{ route('transaksis.update', ['id' => $data->id]) }}" method="POST" class="transaksi" id="add_form" style="margin-bottom:100px">
                        @csrf
                        @method('PUT')
                        <div style="display:grid;grid-template-columns: auto auto; gap:25px;">
                            <div class="col" style="margin-left: 1rem;width: 75%">
                                <div style="width:60%">
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Nama Customer:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" name="name_customer" class="form-control form-control-user" id="name_customer" value="{{ $data -> name_customer }}" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Brand Kendaraan:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" name="brand_kendaraan" class="form-control form-control-user" id="brand_kendaraan" placeholder="Pilih Brand Kendaraan" value="{{ $data -> brand_kendaraan }}" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Tipe:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" name="tipe" class="form-control form-control-user" id="tipe" placeholder="Pilih Tipe Kendaraan" value="{{ $data -> tipe }}" disabled> </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Tanggal Pengerjaan:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="datetime-local" class="form-control form-control-user" id="inputDate" value="{{ $data -> tanggal_pengerjaan }}" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Jenis Pembayaran:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <row>
                                                <col>
                                                <input type="radio" id="lunas" name="jenis-pembayaran" value="lunas" @if($data->jenis_pembayaran == 'Lunas') checked @endif disabled>
                                                <label for="lunas">Lunas</label>
                                                <col />
                                                <col>
                                                <input type="radio" id="dp" name="jenis-pembayaran" value="dp" @if($data->jenis_pembayaran == 'DP') checked @endif style="margin-left:20px;" disabled>
                                                <label for="dp">DP</label>
                                                <col />
                                            </row>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="jumlahDP">
                                            <label for="fname">Jumlah DP:</label><br>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control form-control-user" id="inputDP" name="inputDP" value="{{ $data -> value_dp }}" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group row" id="diskon">
                                        <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                            <label for="fname">Diskon:</label><br>
                                        </div>
                                        <div class="col-sm-12" style="display:grid;grid-template-columns: auto auto; gap:10px;">
                                            <label class="toggle" style="width: 70px;">
                                                <input type="checkbox" id="useDiskon" disabled @if($data->diskon != '0') checked @endif >
                                                <span class="slider"></span>
                                                <span class="labels" data-on="Yes" data-off="No"></span>
                                            </label>
                                            <input type="text" class="form-control form-control-user" name="inputDiskon" id="inputDiskon" value="{{ $data -> diskon }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div id="show_item">

                                    </div>
                                </div>
                            </div>
                            <div>
                                <div style="position:absolute;overflow:hidden;width: 24rem; margin-left: -12rem;height:auto;">
                                    <div class="card shadow mb-4">
                                        <div class="card-body" id="detailNota">
                                            <div style="text-align: center;"><strong>- Detail Nota -</strong></div>
                                            <div style="text-align: center;margin-top: 2vw">
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Customer</strong></p>
                                                    <div style="text-align: end;" id="outputCustomer">{{ $data -> name_customer }}</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Brand Kendaraan</strong></p>
                                                    <div style="text-align: end;" id="outputBrandKendaraan">{{ $data -> brand_kendaraan }}</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Tipe</strong></p>
                                                    <div style="text-align: end;" id="outputType">{{ $data -> tipe }}</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Tanggal Pengerjaan</strong></p>
                                                    <div style="text-align: end;" id="outputDate">{{ $data -> tanggal_pengerjaan }}</div>
                                                </div>
                                                <hr class="rounded" style="margin-top:-0.2vw">
                                                <div style="display:grid;grid-template-columns: auto auto;margin-top:-1.5vw">
                                                    <p style="text-align: start;margin-top:20px"><strong>Jasa</strong></p>
                                                </div>
                                                <div id="outputJasa">

                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;margin-top:20px"><strong>Sparepart</strong></p>
                                                </div>
                                                <div id="outputSparepart">

                                                </div>
                                                <hr class="rounded">
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Total</strong></p>
                                                    <div style="text-align: end;" id="outputTotal">-</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto;" id="outputDiskon">
                                                    <p style="text-align: start;"><strong>Diskon</strong></p>
                                                    <div style="text-align: end;" id="outputValueDiskon">{{$data -> diskon}}</div>
                                                </div>
                                                <hr class="rounded">
                                                <div style="display:grid;grid-template-columns: auto auto; ">
                                                    <p style="text-align: start;"><strong>Grand Total</strong></p>
                                                    <div style="text-align: end;" id="outputGrandTotal">{{$data -> grand_total}}</div>
                                                </div>
                                                <div style="display:grid;grid-template-columns: auto auto;" id="outputDP">
                                                    <p style="text-align: start;"><strong>Down Payment</strong></p>
                                                    <div style="text-align: end;" id="outputValueDP">{{$data -> kekurangan}}</div>
                                                </div>
                                                <hr class="rounded">

                                                <div style="display:grid;grid-template-columns: auto auto;" id="outputPelunasan">
                                                    <p style="text-align: start;"><strong>Pelunasan / Kekurangan</strong></p>
                                                    <div style="text-align: end;" id="outputValuePelunasan">{{$data -> kekurangan}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="jsonData" id="jsonData" style="display: none;">
                                    <input type="hidden" name="dataFromJS" id="dataFromJS" style="display: none;">
                                    <input type="hidden" name="dataGrandPelunasan" id="dataGrandPelunasan" style="display: none;">

                                    <input type="submit" class="btn btn-primary btn-user btn-block" value="Transaksi Pelunasan">
                                    <button class="btn btn-primary btn-user btn-block" onclick="PrintElem()"><i class="fa fa-print" aria-hidden="true"></i> Print Nota</button>
                                    <a href="{{ route('transaksis') }}" class="btn btn-outline-primary btn-user btn-block">
                                        Kembali
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span><strong>Star Oto Service Management </strong> &copy; 2024 Created by Alvian Andro</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->
        </div>
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="{{ route('login') }}" onclick="handleLogout()">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        var data = <?php echo json_encode($row); ?>;
        var jsonData = JSON.stringify(data);
        document.getElementById('dataFromJS').value = jsonData;

        //Pengerjaan
        var pengerjaanArray = JSON.parse(data[0].pengerjaan);
        pengerjaanArray.forEach(function(obj, key) {
            const div = document.createElement('div');
            div.innerHTML = `
                    <div class="card" style="margin-top:10px">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Pengerjaan</h6>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body" style="margin-right: 14rem;">
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;">
                                    <label for="fname">Jenis Pengerjaan:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <input name="inputJenisPengerjaan" type="text" class="form-control form-control-user" id="inputJenisPengerjaan" value="${obj.jenispengerjaan}" disabled>
                                </div>
                            </div>
                            <div class="form-group row" id="containerPengerjaan" style="display:''">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="namaPengerjaan" style="display:''">
                                    <label name="product_name[]" for="fname">Nama Pengerjaan:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <input name="inputNamaPengerjaan" type="text" class="form-control form-control-user" id="inputNamaPengerjaan" value="${obj.namapengerjaan}"  disabled>
                                </div>
                            </div>
                            <div class="form-group row" id="containerSparepart">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end; id="namaSparepart ">
                                    <label for="fname">Nama Sparepart:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <input name="inputNamaSparepart" type="text" class="form-control form-control-user" id="inputNamaSparepart" value="${obj.namasparepart}"  disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="jumlahDP">
                                    <label name="product_name[]" for="fname">Harga:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <input name="inputHarga" type="text" class="form-control form-control-user" id="inputHarga" value="${obj.harga}"  disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="text-align: end;" id="jumlahDP">
                                    <label name="product_name[]" for="fname">Kuantitas:</label><br>
                                </div>
                                <div class="col-sm-12">
                                    <input name="inputKuantitas" type="number" class="form-control form-control-user" id="inputKuantitas" value="${obj.kuantitas}"  disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            document.querySelector('#show_item').append(div);
        });

        let total = []
        pengerjaanArray.forEach(function(obj, key) {
            const div = document.createElement('div');
            if (obj.jenispengerjaan == 'jasa') {
                let harga = obj.harga;
                let kuantitas = obj.kuantitas;
                let price = Number(harga).toLocaleString("id-ID");
                let finalHarga = (Number(harga) * Number(kuantitas)).toLocaleString("id-ID");
                total.push(Number(harga));

                div.innerHTML = `
                        <div style="display:grid;grid-template-columns: auto auto;">
                            <div style="text-align: start;">${obj.namapengerjaan} x ${obj.kuantitas}</div>
                            <div style="text-align: end;">Rp. ${finalHarga}</div>
                            <div style="text-align: start;">&nbsp;&nbsp;@ Rp. ${price}</div>
                        </div>
                    `;
                document.querySelector('#outputJasa').append(div);
            } else {
                let harga = obj.harga;
                let kuantitas = obj.kuantitas;
                let price = Number(harga).toLocaleString("id-ID");
                let finalHarga = (Number(harga) * Number(kuantitas)).toLocaleString("id-ID");
                total.push(Number(harga));

                div.innerHTML = `
                        <div style="display:grid;grid-template-columns: auto auto;">
                            <div style="text-align: start;">${obj.namasparepart} x ${obj.kuantitas}</div>
                            <div style="text-align: end;">Rp. ${finalHarga}</div>
                            <div style="text-align: start;">&nbsp;&nbsp;@ Rp. ${price}</div>
                        </div>
                    `;
                document.querySelector('#outputSparepart').append(div);
            }
        });

        //Total
        var totalElement = document.getElementById("outputTotal");
        var totalValue = total.reduce((partialSum, a) => partialSum + a, 0);
        var formattedTotal = totalValue.toLocaleString("id-ID");

        outputTotal.textContent = 'Rp. ' + formattedTotal;

        //Diskon
        var outputDiskon = document.getElementById("outputDiskon");
        if (data[0].diskon == "" || data[0].diskon == "0") {
            outputDiskon.style.display = 'none'
        };
        var outputValueDiskon = document.getElementById("outputValueDiskon");
        var diskon = parseFloat(outputValueDiskon.textContent);
        var formattedDiskon = diskon.toLocaleString("id-ID");

        outputValueDiskon.textContent = 'Rp. ' + formattedDiskon;

        //Grand Total
        var grandTotalElement = document.getElementById("outputGrandTotal");
        var grandTotal = parseFloat(grandTotalElement.textContent);
        var formattedGrandTotal = grandTotal.toLocaleString("id-ID");

        grandTotalElement.textContent = 'Rp. ' + formattedGrandTotal;

        //Total DP
        var outputValueDP = document.getElementById("outputValueDP");
        var valueDP = parseFloat(outputValueDP.textContent);
        var formattedOutputValueDP = valueDP.toLocaleString("id-ID");

        outputValueDP.textContent = 'Rp. ' + formattedOutputValueDP;

        //Pelunasan atau Kekurangan 
        var outputValuePelunasan = document.getElementById("outputValuePelunasan");
        var valuePelunasan = parseFloat(outputValuePelunasan.textContent);
        var formattedPelunasan = valuePelunasan.toLocaleString("id-ID");

        outputValuePelunasan.textContent = 'Rp. ' + formattedPelunasan;


        function PrintElem() {
            var customer = document.getElementById("outputCustomer").textContent;
            var brand_kendaraan = document.getElementById("outputBrandKendaraan").textContent;
            var tipe = document.getElementById("outputType").textContent;
            var date = document.getElementById("outputDate").textContent;

            var imageUrl = '../../../public.logo.jpeg';
            var base64 = '0';
            var fileName = `Transaksi ${customer} (${brand_kendaraan} ${tipe}) ${date}`;
            var mywindow = window.open('', 'PRINT', 'wheight=400,width=600');

            mywindow.document.write('<html><head>');
            mywindow.document.write('<title>' + fileName + '</title>');
            mywindow.document.write('<style>');
            mywindow.document.write('h1 { text-align: center; }'); // Center the title
            mywindow.document.write('</style>');
            mywindow.document.write('</head><body>');
            mywindow.document.write('<div style="display:grid;grid-template-columns: auto auto;">');
            mywindow.document.write('<img src="../../logo.jpeg" alt="logo" width="200" style="border-radius:10px;" ></img>');
            mywindow.document.write('<h1 style="margin-top: 7vm">Transaksi</h1>');
            mywindow.document.write('</div>');
            mywindow.document.write(document.getElementById("detailNota").innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();

            return true;
        }

        function imageToBase64(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    callback(reader.result);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }
    </script>

    <script>
        <?php
        $conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
        if ($conn->connect_errno) {
            echo "Failed to connect to MySQL: " . $conn->connect_error;
            exit();
        }

        //Retrieve Customer
        $sql = "select * from users";
        $result = ($conn->query($sql));
        $row = [];

        if ($result->num_rows > 0) $row = $result->fetch_all(MYSQLI_ASSOC);
        ?>

        var user = <?php echo json_encode($row); ?>;
        var email = localStorage.getItem("email");
        var username = user.find(obj => obj.email === email).name;
        var userProfile = document.getElementById("userProfile");

        userProfile.textContent = username;

        var token = localStorage.getItem("token");
        if (!token || token === undefined || token === null) {
            window.location.href = "{{ route('login') }}";
        };

        function handleLogout() {
            localStorage.clear();
        };
    </script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js'></script>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('template/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('template/js/sb-admin-2.min.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ asset('template/vendor/chart.js/Chart.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('template/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('template/js/demo/chart-pie-demo.js') }}"></script>

</body>

</html>