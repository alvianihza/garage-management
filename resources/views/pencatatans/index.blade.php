<?php
$conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: " . $conn->connect_error;
    exit();
}

$sql = "SELECT * FROM balances";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_all(MYSQLI_ASSOC);
} else {
    $row = array(
        array("balance" => 0)
    );
}
?>

@extends('layout.app')
@section('header')

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pencatatan - Bimmers Garage</title>
    <link rel="icon" type="images/gif/png" href="logo.png" />

    <!-- Custom fonts for this template-->
    <link href="{{ asset('template/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('template/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>
<style>
    .cancelbtn,
    .deletebtn {
        float: left;
        width: 50%;
    }

    .cancelbtn {
        background-color: #ccc;
        color: black;
    }

    .deletebtn {
        background-color: #f44336;
    }

    .container {
        padding: 16px;
        text-align: center;
    }

    .modal {
        display: none;
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: #ffffff;
    }

    .modal-content {
        background-color: #fefefe;
        margin: 5% auto 15% auto;
        border: 1px solid #888;
        width: 80%;
    }

    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    .close {
        position: absolute;
        right: 35px;
        top: 15px;
        font-size: 40px;
        font-weight: bold;
        color: #f1f1f1;
    }

    .close:hover,
    .close:focus {
        color: #f44336;
        cursor: pointer;
    }

    .clearfix::after {
        content: "";
        clear: both;
        display: table;
    }

    @media screen and (max-width: 300px) {

        .cancelbtn,
        .deletebtn {
            width: 100%;
        }
    }
</style>

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Pencatatan</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3" style="display:grid;grid-template-columns: auto auto; gap:10px;">
        <div class="col-sm-6 mb-3 mb-sm-0" style="margin-top:15px">
            <a href="{{ route('pencatatans.insert') }}" class="btn btn-primary btn-sm" id="tambahButton"><i class="fa fa-plus"> Tambah</i></a>
        </div>
        <div class="col-sm-12">
            <div style="text-align:end;margin-top:2px;font-size:14px;">Saldo &nbsp;&nbsp;</div>
            <strong>
                <div id="outputSaldo" style="text-align:end;color:#4e73df;font-size:18px;">{{$row[0]['balance']}}</div>
            </strong>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Keterangan</th>
                        <th>Tanggal</th>
                        <th>Pemasukkan</th>
                        <th>Pengeluaran</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1 ?>
                    @foreach ($pencatatans as $row)
                    <tr class="text-center">
                        <td>{{ $no++ }}</td>
                        <td>{{ $row->keterangan }}</td>
                        <td>{{ $row->created_at }}</td>
                        <td>{{ number_format($row->income, 0, ',', ',') }}</td>
                        <td>{{ number_format($row->outcome, 0, ',', ',') }}</td>
                        <td>
                            <a href="{{ route('pencatatans.edit', ['id' => $row->id]) }}" class="btn btn-primary btn-sm"> View </a>
                            <button class="btn btn-danger btn-sm" onclick="handleDeleteTable({{ $row->id }});" id="deleteButton"> Delete </i></a>
                        </td>
                    </tr>

                    <div id="id{{ $row->id }}" class="modal" style="position: fixed;top: 0vw; left: 0vw;">
                        <span onclick="document.getElementById('id{{ $row->id }}').style.display='none'" class="close" title="Close Modal">×</span>
                        <div class="container">
                            <h1>Hapus Pencatatan</h1>
                            <p>Apakah anda yakin ingin mengapus pencatatan <b>{{ $row->keterangan }}</b> ?</p>
                            <form action="{{route('pencatatans.delete',['id' =>  $row->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="clearfix">
                                    <button type="button" onclick="document.getElementById('id{{ $row->id }}').style.display='none'" class="cancelbtn">Tidak, Batal</button>
                                    <button type="submit" class="deletebtn">Ya, Hapus</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    //Pelunasan atau Kekurangan 
    var outputSaldo = document.getElementById("outputSaldo");
    var valueSalsod = parseFloat(outputSaldo.textContent);
    var formattedOutputSaldo = valueSalsod.toLocaleString("id-ID");

    outputSaldo.textContent = 'Rp. ' + formattedOutputSaldo;

    function handleDeleteTable(id) {
        document.getElementById(`id${id}`).style.display = 'block';
        modal = id;
    };
</script>

<script>
    <?php
    $conn = new mysqli(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
    if ($conn->connect_errno) {
        echo "Failed to connect to MySQL: " . $conn->connect_error;
        exit();
    }

    //Retrieve Customer
    $sql = "select * from users";
    $result = ($conn->query($sql));
    $row = [];

    if ($result->num_rows > 0) $row = $result->fetch_all(MYSQLI_ASSOC);
    ?>

    var user = <?php echo json_encode($row); ?>;
    var email = localStorage.getItem("email");
    var role = user.find(obj => obj.email === email).role;
    var tambahButton = document.getElementById("tambahButton");
    var deleteButton = document.getElementById("deleteButton");

    if (role === 'admin') {
        tambahButton.style.display = 'none';
        deleteButton.style.display = 'none';
    }
</script>
@endsection